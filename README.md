# libfeedback-rs

The Rust bindings of [feedbackd](https://source.puri.sm/Librem5/feedbackd)'s libfeedback

Website: <https://guidog.pages.gitlab.gnome.org/libfeedback-rs/git/docs/>

## Documentation

- libfeedback: <https://guidog.pages.gitlab.gnome.org/libfeedback-rs/git/docs/libfeedback/>
- libfeedback-sys: <https://guidog.pages.gitlab.gnome.org/libfeedback-rs/git/docs/libfeedback-sys/>
